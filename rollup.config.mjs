import { terser } from "rollup-plugin-terser";
import resolve from "@rollup/plugin-node-resolve";

export default {
    input: "index.mjs",
    output: {
        file: "dist/index.mjs",
        format: "esm"
    },
    external: ["logging"],
    plugins: [
        resolve({ browser: true }),
        process.env.BUILD === "production" && terser()
    ]
};
