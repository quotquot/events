/*
 * Copyright 2020-2021 Johnny Accot
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by the
 * European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *      http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { debounce, throttle } from "lodash-es";
export { debounce, throttle };
import { AttrRenderer, BaseHandler } from "@quotquot/element";
import logging from "logging";

const logger = logging.getLogger("@quotquot/events");
const transforms = { debounce, throttle };


/**
 * Initializes the events module.
 * Examples of event attributes:
 *   - data-on-click="foo"
 *   - data-on-click="foo; bar"
 *   - data-on-click="[debounce=100]foo"
 *   - data-on-click="[throttle=100]bar"
 *   - data-on-click="[debounce=100]foo; [throttle=100]bar"
 * @param {Object} logger A console-like logging object with debug/warn/error methods.
 * @returns {Object} the events rendering object
 */

class EventHandler extends BaseHandler {

    static #ATTR_PREFIX = "data-on-";

    static getAttributeNodes(el) {
        const attrs = [];
        for (const attr of el.attributes)
            if (EventHandler.manages(attr.name))
                attrs.push(attr);
        return attrs.length ? attrs : null;
    }

    static manages(attrName) {
        return attrName.startsWith(EventHandler.#ATTR_PREFIX);
    }

    static extractEventName(attrName) {
        return attrName.slice(8);
    }

    static parseAttrValue(attrValue) {
        const result = [];
        for (const spec of attrValue.split(";")) {
            const m = /^\s*(?:\[(debounce|throttle)=(\d+)\])?([\w-]*)\s*$/u.exec(spec);
            if (!m)
                throw new Error(`incorrect event format: "${spec}"`);
            // if m[2] is null, parseInt returns NaN
            result.push([m[3], m[1], parseInt(m[2], 10)]);
        }
        return result;
    }

    #el;
    #definitions;
    #subscriptions;

    constructor(renderer, el, parent, attrs) {
        super(parent);
        this.#el = el;
        this.#definitions = {};
        this.#subscriptions = new Map();
        for (const attr of attrs) {
            const eventName = EventHandler.extractEventName(attr.name);
            this.#definitions[eventName] = EventHandler.parseAttrValue(attr.value);
        }
    }

    async #render(manager, newState) {
        const values = this.getValues(newState);
        for (const [eventName, definition] of Object.entries(this.#definitions)) {
            const callbacks = [];
            for (const [callbackName, transformName, transformParam] of definition) {
                let callback = values.events[callbackName];
                if (!callback)
                    throw new Error(`event handler "${callbackName}" not in state`);

                logger.debug(`binding ${eventName} to ${callbackName}`);

                if (transformName)
                    callback = transforms[transformName](callback, transformParam);

                this.#el.addEventListener(eventName, callback);
                callbacks.push(callback);
            }
            this.#subscriptions.set(eventName, callbacks);
        }
    }

    async render(manager, newState, context) {
        this.setContext(context);
        await this.#render(manager, newState);
    }

    async update(manager, oldState, newState, changed) {
        if (changed.has("events")) {
            this.clear();
            await this.#render(manager, newState);
        }
    }

    clear() {
        for (const eventName of Object.keys(this.#subscriptions)) {
            logger.debug(`unbinding ${eventName}`);
            for (const callback of this.#subscriptions.get(eventName))
                this.#el.removeEventListener(eventName, callback);
            this.#subscriptions.delete(eventName);
        }
    }
}

export default class EventRenderer extends AttrRenderer {
    constructor() {
        super(EventHandler);
    }
}
