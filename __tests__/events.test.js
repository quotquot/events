/* eslint no-return-assign: off */
/* eslint require-jsdoc: off */
/* eslint no-undef: off */
/* eslint no-console: off */

import { State, RenderingManager } from "@quotquot/element/dist/index.mjs";
import EventsRenderer from "../index.mjs";

test("Create events-test element", async() => {

    let event;
    const state = new State({
        events: {
            "": e => event = 0,
            click1: e => event = 1,
            click2: e => event = 2
        }
    });

    const renderer = new EventsRenderer();
    await renderer.setup(document.body, state);

    const manager = new RenderingManager();
    await manager.setup(document.body, state);
    manager.addRenderer(renderer);

    const template = document.createElement("template");
    template.innerHTML = '<div data-on-click="click1"></div>';
    await manager.appendTemplate(template, state.getAll());
    document.body.firstElementChild.click();
    expect(event).toBe(1);

    /* change the attribute value directly; check that the attribute node is the same;
     * then click on the div and check that the event handler was indeed updated */
    template.innerHTML = template.innerHTML.replaceAll("click1", "click2");
    manager.clear();
    await manager.appendTemplate(template, state.getAll());
    document.body.firstElementChild.click();
    expect(event).toBe(2);

    /* change the value back to click1 */
    template.innerHTML = template.innerHTML.replaceAll("click2", "click1");
    manager.clear();
    await manager.appendTemplate(template, state.getAll());
    document.body.firstElementChild.click();
    expect(event).toBe(1);

    /* change the value to empty */
    template.innerHTML = template.innerHTML.replaceAll("click1", "");
    manager.clear();
    await manager.appendTemplate(template, state.getAll());
    document.body.firstElementChild.click();
    expect(event).toBe(0);

    /* change the empty callback and click on the div; the new callback will be used */
    await state.update({ events: { "": e => event = -1 } });
    document.body.firstElementChild.click();
    expect(event).toBe(-1);

    manager.clear();
    await manager.cleanup(document.body, state);
    expect(document.body.innerHTML).toBe("");
});
